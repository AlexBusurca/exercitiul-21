package Ro.Orange;

public class Employee {
    private int id = 0, age = 0;
    private String name = null;

    public Employee (){

    }

    public Employee(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return "Student [ID:" + getId() + " Name:" + getName() + " Age:" + getAge() +"]";
    }
}
