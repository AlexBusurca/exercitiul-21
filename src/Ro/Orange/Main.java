package Ro.Orange;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        
        List<Employee> employees = new ArrayList<>();

        employees.add( new Employee(1,"Jon",20));
        employees.add(new Employee(2,"Steve",23));
        employees.add(new Employee(3,"Kevin",19));
        employees.add(new Employee(4,"Ron",18));
        employees.add(new Employee(5,"Lucy",22));

        System.out.println("Before sorting the data: ");
        for(Employee employee:employees){
            System.out.println( employee.toString());
        }

        Collections.sort(employees,(o1,o2) ->o1.getAge()-o2.getAge());

        System.out.println("After sort by age:");

        for(Employee employee:employees) {
            System.out.println(employee.toString());
        }

        Collections.sort(employees,(o1,o2) ->o1.getName().compareTo(o2.getName()));

        System.out.println("After sort by name:");

        for(Employee employee:employees) {
            System.out.println(employee.toString());
        }

        Collections.sort(employees,(o1,o2) ->o1.getId()-o2.getId());

        System.out.println("After sort by id:");

        for(Employee employee:employees) {
            System.out.println(employee.toString());
        }






    }
}
